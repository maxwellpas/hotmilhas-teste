<?php 
require_once dirname(__FILE__) . '/../model/Banco.php'; 

class Usuarios
{
	private $_table = 'hotfinanc_usuarios';

	private $_nome;
	private $_email;
	private $_senha;
	private $_nivel;
	private $_status;
	

	public function __construct($nome = "", $email = "", $senha = "", $nivel = "", $status = "")
	{
		$this->_nome 	= addslashes($nome);
		$this->_email 	= addslashes($email);
		$this->_senha 	= md5($senha);
		$this->_nivel 	= addslashes($nivel);
		$this->_status 	= addslashes($status);

	}


	public function insert()
	{
		$query = "INSERT INTO " . $this->_table . " 
					(`nome`, `email`, `senha`, `nivel`, `status`) 
						VALUES 
					('".$this->_nome."', '".$this->_email."', '".$this->_senha."', '".$this->_nivel."', '".$this->_status."')";

		$banco = new Banco();
		return $banco->executaSql($query);

	}


	public function existeUsuario()
	{
		$query = "SELECT `id` FROM " . $this->_table . " WHERE `email` = '".$this->_email."' AND `lixo` = '0' ";
		$banco = new Banco();
		return $banco->executaSqlRetornaLinhas($query);

	}


    public function update($id)
    {
        if( !empty($this->_senha) ){
            $query = "UPDATE "  . $this->_table . "
                        SET
                    `nome` = '" . $this->_nome . "',
                    `email` = '" . $this->_email . "',
                    `senha` = '" . $this->_senha . "',
                    `nivel` = '" . $this->_nivel . "',
                    `status` = '" . $this->_status . "'
                WHERE `id` = '".$id."'
                ";

        } else {
            $query = "UPDATE "  . $this->_table . "
                        SET
                    `nome` = '" . $this->_nome . "',
                    `email` = '" . $this->_email . "',
                    `nivel` = '" . $this->_nivel . "',
                    `status` = '" . $this->_status . "'
                WHERE `id` = '".$id."'
                ";
        }

        $banco = new Banco();
        return $banco->executaSql($query);

    }


	public function buscarTodos()
	{
		$query = "SELECT `id`,`nome`,`email`,`status`, `nivel` FROM " . $this->_table . " WHERE `lixo` = '0' ";
		$banco = new Banco();
		return $banco->executaSqlRetorna($query);
	}


    public function buscarPorId($id)
    {
        $id = addslashes($id);

        $query = "SELECT `id`,`nome`,`email`,`senha`, `nivel`, `status` FROM " . $this->_table . " WHERE `id` = '".$id."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);

    }


    public function buscarPorEmail($email)
    {
        $email = addslashes($email);

        $query = "SELECT `id`,`nome`,`email`,`senha`, `nivel`, `status` FROM " . $this->_table . " WHERE `email` = '".$email."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);

    }


    public function delete($id)
    {
        $query = "UPDATE "  . $this->_table . " SET `lixo` = '1' WHERE `id` = '".$id."' ";
        $banco = new Banco();
        return $banco->executaSql($query);

    }


    public function confirmaUsuario($email, $senha)
    {
        $email = addslashes($email);
        $senha = md5($senha);

        $query = "SELECT `id` FROM " . $this->_table . " WHERE `email` = '".$email."' AND `senha` = '".$senha."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetornaLinhas($query);
    }


    public function atualizaDataDoLogin($email)
    {
        $query = "UPDATE "  . $this->_table . " SET `data_ultimo_login` = 'NOW()' WHERE `email` = '".$email."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSql($query);

    }

}
