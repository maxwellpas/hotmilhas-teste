<?php 
require_once dirname(__FILE__) . '/../model/Banco.php'; 

class Status
{
	private $_table = 'hotfinanc_status_pgto';

	private $_titulo;
	private $_status;
	

	public function __construct($titulo = "", $status = "")
	{
		$this->_titulo 	        = addslashes($titulo);
		$this->_status 	            = addslashes($status);

	}


	public function buscarTodos()
	{
		$query = "SELECT `id`,`titulo` FROM " . $this->_table . " WHERE `lixo` = '0' ";
		$banco = new Banco();
		return $banco->executaSqlRetorna($query);
	}


    public function buscarPorId($id)
    {
        $id = addslashes($id);

        $query = "SELECT `id`,`titulo` FROM " . $this->_table . " WHERE `id` = '".$id."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);

    }




}
