<?php 
require_once dirname(__FILE__) . '/../model/Banco.php'; 

class Transacao
{
	private $_table = 'hotfinanc_transacoes';
    private $_table_conta = 'hotfinanc_contas';
    private $_table_status = 'hotfinanc_status_pgto';
    private $_table_usuarios = 'hotfinanc_usuarios';

	private $_id_conta;
    private $_titulo;
	private $_valor;
	private $_id_status_pgto;
	private $_data_prevista;
	private $_data_realizada;
	

	public function __construct($id_conta = "", $titulo = "", $valor = "", $id_status_pgto = "", $data_prevista = "", $data_realizada = "")
	{
		$this->_id_conta 	        = addslashes($id_conta);
        $this->_titulo  	        = addslashes($titulo);
		$this->_valor 	            = addslashes($valor);
		$this->_id_status_pgto 	    = addslashes($id_status_pgto);
		$this->_data_prevista 	    = addslashes($data_prevista);
		$this->_data_realizada 	    = addslashes($data_realizada);

	}


	public function insert()
	{
		$query = "INSERT INTO " . $this->_table . " 
					(`id_conta`, `titulo`, `valor`, `id_status_pgto`, `data_prevista`, `data_realizada`)
						VALUES 
					('".$this->_id_conta."', '".$this->_titulo."', '".$this->_valor."', '".$this->_id_status_pgto."', '".$this->_data_prevista."', '".$this->_data_realizada."')";

		$banco = new Banco();
		return $banco->executaSql($query);

	}


	public function existeConta()
	{
		$query = "SELECT `id` FROM " . $this->_table . " WHERE `data_prevista` = '".$this->_data_prevista."' AND `lixo` = '0' ";
		$banco = new Banco();
		return $banco->executaSqlRetornaLinhas($query);

	}


    public function update($id)
    {
        $query = "UPDATE "  . $this->_table . "
                    SET
                `id_conta` = '" . $this->_id_conta . "',
                `titulo` = '" . $this->_titulo . "',
                `valor` = '" . $this->_valor . "',
                `id_status_pgto` = '" . $this->_id_status_pgto . "',
                `data_prevista` = '" . $this->_data_prevista . "',
                `data_realizada` = '" . $this->_data_realizada . "',
                `data_edicao` = 'NOW()'
            WHERE `id` = '".$id."'
            ";
//die('foi');

        $banco = new Banco();
        return $banco->executaSql($query);

    }


	public function buscarTodos()
	{
		$query = "SELECT
                    tra.`id` as 'id_transacao',
                    tra.`titulo` as 'titulo_transacao',
                    tra.`valor` as 'valor_transacao',
                    tra.`data_prevista` as 'data_prevista_transacao',
                    tra.`data_realizada` as 'data_realizada_transacao',
                    con.`id` as 'id_conta', con.`empresa` as 'empresa_conta',
                    sta.`id` as 'id_status_pgto', sta.`titulo` as 'titulo_status_pgto'
                  FROM " . $this->_table . " as tra
                    LEFT JOIN ". $this->_table_conta." as con ON con.`id` = tra.`id_conta`
                    LEFT JOIN ". $this->_table_status." as sta ON tra.`id_status_pgto` = sta.`id`
                  WHERE tra.`lixo` = '0' ";
		$banco = new Banco();
		return $banco->executaSqlRetorna($query);
	}


    public function buscarTodosPorUsuario($id_usuario)
    {
        $query = "SELECT
                    tra.`id` as 'id_transacao',
                    tra.`titulo` as 'titulo_transacao',
                    tra.`valor` as 'valor_transacao',
                    tra.`data_prevista` as 'data_prevista_transacao',
                    tra.`data_realizada` as 'data_realizada_transacao',
                    con.`id` as 'id_conta', con.`empresa` as 'empresa_conta',
                    sta.`id` as 'id_status_pgto', sta.`titulo` as 'titulo_status_pgto'
                  FROM " . $this->_table . " as tra
                    LEFT JOIN ". $this->_table_conta." as con ON con.`id` = tra.`id_conta`
                    LEFT JOIN ". $this->_table_usuarios." as u ON u.`id` = con.`id_usuario`
                    LEFT JOIN ". $this->_table_status." as sta ON tra.`id_status_pgto` = sta.`id`
                  WHERE tra.`lixo` = '0' AND con.`id_usuario` = '".$id_usuario."'";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);
    }


    public function pesquisa($busca)
    {
        $query = "SELECT
                    tra.`id` as 'id_transacao',
                    tra.`titulo` as 'titulo_transacao',
                    tra.`valor` as 'valor_transacao',
                    tra.`data_prevista` as 'data_prevista_transacao',
                    tra.`data_realizada` as 'data_realizada_transacao',
                    con.`id` as 'id_conta', con.`empresa` as 'empresa_conta',
                    sta.`id` as 'id_status_pgto', sta.`titulo` as 'titulo_status_pgto'
                  FROM " . $this->_table . " as tra
                    LEFT JOIN ". $this->_table_conta." as con ON con.`id` = tra.`id_conta`
                    LEFT JOIN ". $this->_table_status." as sta ON tra.`id_status_pgto` = sta.`id`
                  WHERE
                  tra.`lixo` = '0' AND
                  (".$busca.")
                  ";

        $banco = new Banco();
        return $banco->executaSqlRetorna($query);
    }


    public function buscarPorId($id)
    {
        $id = addslashes($id);

        $query = "SELECT `id`,`id_conta`, `titulo`,`valor`,`id_status_pgto`, `data_prevista`, `data_realizada` FROM " . $this->_table . " WHERE `id` = '".$id."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);

    }


    public function delete($id)
    {
        $query = "UPDATE "  . $this->_table . " SET `lixo` = '1' WHERE `id` = '".$id."' ";
        $banco = new Banco();
        return $banco->executaSql($query);

    }




}
