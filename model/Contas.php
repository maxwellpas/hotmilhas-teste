<?php 
require_once dirname(__FILE__) . '/../model/Banco.php'; 

class Contas
{
	private $_table = 'hotfinanc_contas';
    private $_table_user = 'hotfinanc_usuarios';

	private $_id_usuario;
    private $_nome_responsavel;
	private $_empresa;
	private $_telefone;
	private $_cnpj;
	private $_status;
	

	public function __construct($id_usuario = "", $nome_responsavel = "", $empresa = "", $telefone = "", $cnpj = "", $status = "")
	{
		$this->_id_usuario 	        = addslashes($id_usuario);
        $this->_nome_responsavel 	= addslashes($nome_responsavel);
		$this->_empresa 	        = addslashes($empresa);
		$this->_telefone 	        = addslashes($telefone);
		$this->_cnpj 	            = addslashes($cnpj);
		$this->_status 	            = addslashes($status);

	}


	public function insert()
	{
		$query = "INSERT INTO " . $this->_table . " 
					(`id_usuario`, `nome_responsavel`, `empresa`, `telefone`, `cnpj`, `status`)
						VALUES 
					('".$this->_id_usuario."', '".$this->_nome_responsavel."', '".$this->_empresa."', '".$this->_telefone."', '".$this->_cnpj."', '".$this->_status."')";

		$banco = new Banco();
		return $banco->executaSql($query);

	}


	public function existeConta()
	{
		$query = "SELECT `id` FROM " . $this->_table . " WHERE `cnpj` = '".$this->_cnpj."' AND `lixo` = '0' ";
		$banco = new Banco();
		return $banco->executaSqlRetornaLinhas($query);

	}


    public function update($id)
    {
        $query = "UPDATE "  . $this->_table . "
                    SET
                `id_usuario` = '" . $this->_id_usuario . "',
                `nome_responsavel` = '" . $this->_nome_responsavel . "',
                `empresa` = '" . $this->_empresa . "',
                `telefone` = '" . $this->_telefone . "',
                `cnpj` = '" . $this->_cnpj . "',
                `status` = '" . $this->_status . "'
            WHERE `id` = '".$id."'
            ";


        $banco = new Banco();
        return $banco->executaSql($query);

    }


	public function buscarTodos()
	{
		$query = "SELECT
                    CO.`id`, CO.`id_usuario`,U.`nome` as `nome_user`, CO.`nome_responsavel`,CO.`empresa`,CO.`telefone`, CO.`cnpj`, CO.`status`
                    FROM
                    " . $this->_table . " AS CO LEFT JOIN ". $this->_table_user." AS U ON CO.`id_usuario` = U.`id`
                    WHERE
                    CO.`lixo` = '0' ";
//        die;

		$banco = new Banco();
		return $banco->executaSqlRetorna($query);
	}


    public function buscarTodosPorUser($id_usuario)
    {
        $query = "SELECT
                    CO.`id`, CO.`id_usuario`,U.`nome` as `nome_user`, CO.`nome_responsavel`,CO.`empresa`,CO.`telefone`, CO.`cnpj`, CO.`status`
                    FROM
                    " . $this->_table . " AS CO LEFT JOIN ". $this->_table_user." AS U ON CO.`id_usuario` = U.`id`
                    WHERE
                    CO.`id_usuario` = '".$id_usuario."' AND CO.`lixo` = '0' ";
//        die;

        $banco = new Banco();
        return $banco->executaSqlRetorna($query);
    }



    public function buscarTodosAtvos()
    {
        $query = "SELECT `id`, `id_usuario`, `nome_responsavel`,`empresa`,`telefone`, `cnpj` FROM " . $this->_table . " WHERE `status` = '1' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);
    }


    public function buscarPorId($id)
    {
        $id = addslashes($id);

        $query = "SELECT `id`, `id_usuario`, `nome_responsavel`,`empresa`,`telefone`, `cnpj`, `status` FROM " . $this->_table . " WHERE `id` = '".$id."' AND `lixo` = '0' ";
        $banco = new Banco();
        return $banco->executaSqlRetorna($query);

    }


    public function delete($id)
    {
        $query = "UPDATE "  . $this->_table . " SET `lixo` = '1' WHERE `id` = '".$id."' ";
        $banco = new Banco();
        return $banco->executaSql($query);

    }




}
