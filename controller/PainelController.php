<?php 

if (!session_id()) {
	session_start();

}

require_once dirname(__FILE__) . '/../controller/HelperController.php';

class PainelController
{
    protected $_helper;

	public function __construct()
	{
	    $this->_helper = new HelperController();
        $this->_helper->userLogin();
	}
	

	public function index()
	{
		require_once 'view/painel/index.php';
		return ;
	}


	public function logout()
	{
        session_destroy();
		header("LOCATION: /");
        exit;
	}
}