<?php 
if (!session_id()) {
	session_start();

}

require_once dirname(__FILE__) . '/../model/Usuarios.php';

class HomeController
{
    private $_actionIndex = '/?class=Home&acao=index';
    private $_actionPainel = '/?class=painel&acao=index';

	public function __construct()
	{

	}
	

	public function index()
	{
		require_once 'view/home/index.php';
		return ;
	}


    public function login()
    {
        $_SESSION['campos'] = $_POST;

        if( empty($_POST['email']) || empty($_POST['senha']) ){
            $_SESSION['message'] = ['txt' => 'Campos de e-mail e senha são obrigatórios', 'status' => 'error'];
            header("LOCATION: " . $this->_actionIndex);
            exit();
        }

        $usuarios = new Usuarios();

        if( $usuarios->confirmaUsuario($_POST['email'], $_POST['senha']) > 0 ){

            $usuarios->atualizaDataDoLogin($_POST['email']);

            $retorno    = $usuarios->buscarPorEmail($_POST['email']);

            $dados      = $retorno->fetch_assoc();


            $_SESSION['user_login']['id']       = $dados['id'];
            $_SESSION['user_login']['nome']     = $dados['nome'];
            $_SESSION['user_login']['email']    = $dados['email'];
            $_SESSION['user_login']['nivel']    = $dados['nivel'];
            $_SESSION['user_login']['status']   = $dados['id'];

            header("LOCATION: " . $this->_actionPainel);
            exit();

        }


        $_SESSION['message'] = ['txt' => 'Não existe usuário com os dados informados', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();




    }

}