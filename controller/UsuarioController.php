<?php
if (!session_id()) {
	session_start();

}

require_once dirname(__FILE__) . '/../model/Usuarios.php';
require_once dirname(__FILE__) . '/../controller/HelperController.php';

class UsuarioController
{

	private $_actionCreate = '/?class=usuario&acao=create';
	private $_actionIndex = '/?class=usuario&acao=index';
    private $_actionEdit = '/?class=usuario&acao=edit';

    protected $_helper;


	public function __construct()
	{
        $this->_helper = new HelperController();
        $this->_helper->userLogin();
	}


    /**
     * @param $redirect
     * @param bool $update
     * Valida os campos via posts
     */
    public function validaCampos($redirect, $update = false)
    {
        $_SESSION['campos'] = $_POST;

//         echo '<pre>';
//         print_r($_POST);
//         echo '</pre>';
//        echo $update;
//         die();

        if($update){

            if( empty($_POST['nome']) || empty($_POST['email']) || empty($_POST['email_confirm']) || empty($_POST['nivel']) ){
                $_SESSION['message'] = ['txt' => 'Todos os campos são obrigatórios', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();

            }

            if($_POST['email'] != $_POST['email_confirm']){
                $_SESSION['message'] = ['txt' => 'Os e-mails devem ser iguais', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();
            }

            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $_SESSION['message'] = ['txt' => 'Informar um e-mail válido.', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();

            }

            if( !empty($_POST['senha']) || !empty($_POST['senha_confirm']) ){


                if( empty($_POST['senha']) || empty($_POST['senha_confirm']) ){
                    $_SESSION['message'] = ['txt' => 'Se vai alterar a senha, os campos de senhas são obritatórios', 'status' => 'error'];
                    header("LOCATION: " . $redirect);
                    exit();
                }

                if($_POST['senha'] != $_POST['senha_confirm']){
                    $_SESSION['message'] = ['txt' => 'As senhas devem ser iguais', 'status' => 'error'];
                    header("LOCATION: " . $redirect);
                    exit();
                }

                if( strlen($_POST['senha']) < 6 ){
                    $_SESSION['message'] = ['txt' => 'A senha deve ter no mínimo 6 caracteres.', 'status' => 'error'];
                    header("LOCATION: " . $redirect);
                    exit();
                }

            }



        } else {
            if( empty($_POST['nome']) || empty($_POST['email']) || empty($_POST['email_confirm'])
                || empty($_POST['senha']) || empty($_POST['senha_confirm']) || empty($_POST['nivel']) ){
                $_SESSION['message'] = ['txt' => 'Todos os campos são obrigatórios', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();

            }

            if($_POST['email'] != $_POST['email_confirm']){
                $_SESSION['message'] = ['txt' => 'Os e-mails devem ser iguais', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();
            }

            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $_SESSION['message'] = ['txt' => 'Informar um e-mail válido.', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();

            }


            if($_POST['senha'] != $_POST['senha_confirm']){
                $_SESSION['message'] = ['txt' => 'As senhas devem ser iguais', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();
            }

            if( strlen($_POST['senha']) < 6 ){
                $_SESSION['message'] = ['txt' => 'A senha deve ter no mínimo 6 caracteres.', 'status' => 'error'];
                header("LOCATION: " . $redirect);
                exit();
            }

        }




    }


	public function index()
	{
		$usuarios 	= new Usuarios();
		$retorno 	= $usuarios->buscarTodos();
		require_once "view/usuario/index.php";
		exit();
	}


	public function create()
	{
		require_once "view/usuario/create.php";
		exit();

	}


	public function store()
	{

        $this->validaCampos($this->_actionCreate);

//        die('passou');

		$usuarios 		= new Usuarios($_POST['nome'], $_POST['email'], $_POST['senha'], $_POST['nivel'], 1);
		$retornoUser 	= $usuarios->existeUsuario(); // verifica se existe usuário com mesmo email

		if( $retornoUser == 0 ){

			$retorno 	= $usuarios->insert();
//            echo "<pre>";
//            print_r($retorno);
//            echo "</pre>";
//            die('veio');

			if($retorno['status']){
				$_SESSION['message'] = ['txt' => "Usuário cadastrado com sucesso!"];
				header("LOCATION: " . $this->_actionIndex);
				exit();
			}


			$_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
			header("LOCATION: " . $this->_actionCreate);
			exit();

		}

		$_SESSION['message'] = ['txt' => 'Usuário já existe no banco de dados', 'status' => 'error'];
		header("LOCATION: " . $this->_actionCreate);
		exit();

	}


    public function edit()
    {
        if( isset($_GET['id']) && !empty($_GET['id']) ){
            $usuarios 	= new Usuarios();
            $retorno 	= $usuarios->buscarPorId($_GET['id']);
            $dados      = $retorno->fetch_assoc();
            require_once "view/usuario/edit.php";
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();


    }


    public function update()
    {
        if( isset($_POST['id']) && !empty($_POST['id']) ){

            $action = $this->_actionEdit . "&id=".$_POST['id'];
            $this->validaCampos($action, true);


            $usuarios 		= new Usuarios($_POST['nome'], $_POST['email'], $_POST['senha'], $_POST['nivel'], $_POST['status']);
            $retorno 	    = $usuarios->update($_POST['id']);

            if($retorno['status']){
                $_SESSION['message'] = ['txt' => "Usuário atualizado com sucesso!"];
                header("LOCATION: " . $this->_actionIndex);
                exit();
            }


            $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
            header("LOCATION: " . $this->_actionCreate);
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();

    }


    public function delete()
    {
        if( isset($_GET['id']) && !empty($_GET['id']) ){
            $usuarios 	= new Usuarios();
            $retorno 	= $usuarios->delete($_GET['id']);

            if($retorno['status']){
                $_SESSION['message'] = ['txt' => "Usuário deletado com sucesso!"];
                header("LOCATION: " . $this->_actionIndex);
                exit();
            }


            $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
            header("LOCATION: " . $this->_actionIndex);
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();

    }
}
