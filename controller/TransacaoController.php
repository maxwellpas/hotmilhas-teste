<?php
if (!session_id()) {
	session_start();

}

require_once dirname(__FILE__) . '/../model/Transacao.php';
require_once dirname(__FILE__) . '/../model/Contas.php';
require_once dirname(__FILE__) . '/../model/Status.php';
require_once dirname(__FILE__) . '/../controller/HelperController.php';

class TransacaoController
{

	private $_actionCreate = '/?class=transacao&acao=create';
	private $_actionIndex = '/?class=transacao&acao=index';
    private $_actionEdit = '/?class=transacao&acao=edit';

    protected $_helper;


	public function __construct()
	{
        $this->_helper = new HelperController();
        $this->_helper->userLogin();
	}


    /**
     * @param $redirect
     * @param bool $update
     * Valida os campos via posts
     */
    public function validaCampos($redirect)
    {
        $_SESSION['campos'] = $_POST;

        if( empty($_POST['id_conta']) || empty($_POST['titulo']) || empty($_POST['valor']) || empty($_POST['id_status_pgto']) || empty($_POST['data_prevista']) ){
            $_SESSION['message'] = ['txt' => 'Todos os campos com <strong>*</strong> obrigatórios', 'status' => 'error'];
            header("LOCATION: " . $redirect);
            exit();

        }


    }


	public function index()
	{
		$transacao 	= new Transacao();

        if($_SESSION['user_login']['nivel'] == 1) { // admin
            $retorno 	= $transacao->buscarTodos();

        } else {
            $retorno 	= $transacao->buscarTodosPorUsuario($_SESSION['user_login']['id']);


        }

		require_once "view/transacao/index.php";
		exit();
	}


	public function create()
	{
        $retornoContas = new Contas();

        if($_SESSION['user_login']['nivel'] == 1) { // admin
            $contas 	= $retornoContas->buscarTodos();

        } else {
            $contas 	= $retornoContas->buscarTodosPorUser($_SESSION['user_login']['id']);


        }

//        $contas          = $retornoContas->buscarTodosAtvos();

        $retornoStatus  = new Status();
        $status         = $retornoStatus->buscarTodos();

		require_once "view/transacao/create.php";
		exit();

	}


	public function store()
	{
//        echo "<pre>";
//        print_r($_POST);
//        echo "</pre>";
////        die;

        $this->validaCampos($this->_actionCreate);

//        die('pasou');

        $valor          = str_replace(array('.', ','), array('', '.'), $_POST['valor']);
        $data_prevista  = $this->_helper->inverterData($_POST['data_prevista'], "/", "-");
        $data_realizada = !empty($_POST['data_realizada']) ? $this->_helper->inverterData($_POST['data_realizada'], "/", "-") : 'NULL';

//        die('chec');

		$transacao 	= new Transacao($_POST['id_conta'], $_POST['titulo'], $valor, $_POST['id_status_pgto'], $data_prevista, $data_realizada);
        $retorno 	= $transacao->insert();

        if($retorno['status']){
            $_SESSION['message'] = ['txt' => "Conta cadastrado com sucesso!"];
            header("LOCATION: " . $this->_actionIndex);
            exit();
        }


        $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
        header("LOCATION: " . $this->_actionCreate);
        exit();



	}


    public function edit()
    {
        if( isset($_GET['id']) && !empty($_GET['id']) ){

            $retornoContas = new Contas();
            $contas          = $retornoContas->buscarTodosAtvos();

            $retornoStatus  = new Status();
            $status         = $retornoStatus->buscarTodos();


            $transacao 	= new Transacao();
            $retorno 	= $transacao->buscarPorId($_GET['id']);
            $dados      = $retorno->fetch_assoc();
            require_once "view/transacao/edit.php";
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();


    }


    public function update()
    {
        if( isset($_POST['id']) && !empty($_POST['id']) ){

            $action = $this->_actionEdit . "&id=".$_POST['id'];
            $this->validaCampos($action, true);


            $valor          = str_replace(array('.', ','), array('', '.'), $_POST['valor']);
            $data_prevista  = $this->_helper->inverterData($_POST['data_prevista'], "/", "-");
            $data_realizada = !empty($_POST['data_realizada']) ? $this->_helper->inverterData($_POST['data_realizada'], "/", "-") : 'NULL';


            $transacao 		= new Transacao($_POST['id_conta'], $_POST['titulo'], $valor, $_POST['id_status_pgto'], $data_prevista, $data_realizada);
            $retorno 	    = $transacao->update($_POST['id']);

            if($retorno['status']){
                $_SESSION['message'] = ['txt' => "Transação atualizada com sucesso!"];
                header("LOCATION: " . $this->_actionIndex);
                exit();
            }


            $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
            header("LOCATION: " . $this->_actionCreate);
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();

    }


    public function delete()
    {
        if( isset($_GET['id']) && !empty($_GET['id']) ){
            $transacao 	= new Transacao();
            $retorno 	= $transacao->delete($_GET['id']);

            if($retorno['status']){
                $_SESSION['message'] = ['txt' => "Transação deletada com sucesso!"];
                header("LOCATION: " . $this->_actionIndex);
                exit();
            }


            $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
            header("LOCATION: " . $this->_actionIndex);
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();

    }
}
