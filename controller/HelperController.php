<?php
if (!session_id()) {
	session_start();

}

class HelperController
{
	public function __construct()
	{

	}


    public function userLogin()
    {
        if( !isset($_SESSION['user_login']) ){
            $_SESSION['message'] = ['txt' => 'Login expirado. Você deve fazer seu login para continuar.', 'status' => 'error'];
            header("LOCATION: /");
            exit();
        }
    }


	public function showMessage()
	{
        if( isset($_SESSION['message']) ){
            $html = '';
            $status = !isset($_SESSION['message']['status']) ? 'success' : $_SESSION['message']['status'];

            switch ($status) {
				case 'error':
					$html .= '<div class="alert alert-danger" role="alert">';
					$html .= $_SESSION['message']['txt'];
					$html .='</div>';
					break;

				default:
					$html .= '<div class="alert alert-success" role="alert">';
					$html .= $_SESSION['message']['txt'];
					$html .='</div>';
					unset($_SESSION['campos']);
					break;
			}

			echo $html;
			unset($_SESSION['message']);

		}

	}


	public function showCampos($value)
	{
		if( isset($_SESSION['campos']) && isset($_SESSION['campos'][$value]) && !empty($_SESSION['campos'][$value]) ){
			echo $_SESSION['campos'][$value];
		}

	}


    public function showStatus($status)
    {
        return $status == 1 ? '<span class="badge badge-success">Ativo</span>' : '<span class="badge badge-danger">Desativado</span>';

    }


    public function showNivel($status)
    {
        return $status == 1 ? '<span class="badge badge-light">Nível: Administrador</span>' : '<span class="badge badge-light">Nível: Usuário</span>';

    }


    public function inverterData($data, $separador1, $separador2, $hora = false)
    {
        if (!is_null($data) && !empty($data) && ($data != '0000-00-00')) {
            $data = strstr($data, " ") ? explode(" ", $data) : array($data);

            $dia_mes_ano = explode($separador1, $data[0]);
            $hora = ($hora) ? $data[1] : '';
            return $dia_mes_ano[2] . $separador2 . $dia_mes_ano[1] . $separador2 . $dia_mes_ano[0] . " " . $hora;
        } else {
            return '00/00/0000';
        }
    }

    public function blockNivelUser($dados, $valor, $sessionID)
    {
        if( $dados['id'] != $sessionID){

            if($dados['nivel'] == $valor){
                return 'selected';
            }

        } else {

            if($dados['nivel'] == $valor){
                return 'selected';

            } else {
                return 'disabled';

            }

        }
    }


}
