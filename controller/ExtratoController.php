<?php
if (!session_id()) {
	session_start();

}

require_once dirname(__FILE__) . '/../model/Transacao.php';
require_once dirname(__FILE__) . '/../model/Contas.php';
require_once dirname(__FILE__) . '/../model/Status.php';
require_once dirname(__FILE__) . '/../controller/HelperController.php';

class ExtratoController
{

//	private $_actionCreate = '/?class=transacao&acao=create';
	private $_actionIndex = '/?class=transacao&acao=index';
//    private $_actionEdit = '/?class=transacao&acao=edit';

    protected $_helper;


	public function __construct()
	{
        $this->_helper = new HelperController();
        $this->_helper->userLogin();
	}





	public function index()
	{
        $retornoContas = new Contas();
        $contas          = $retornoContas->buscarTodosAtvos();

        $retornoStatus  = new Status();
        $status         = $retornoStatus->buscarTodos();

        $transacao 	= new Transacao();
		$retorno 	= $transacao->buscarTodos();
		require_once "view/extrato/index.php";
		exit();
	}


    public function buscar()
    {
//        echo "<pre>";
//        print_r($_POST);
//        echo "</pre>";

        $busca = "";

        if( !empty($_POST['id_conta']) ){
            $busca .=  "tra.`id_conta` = '" . addslashes($_POST['id_conta']) . "'";
        }



        if( !empty($_POST['data_prevista_inicio']) ){
            if( !empty($busca) ){
                $busca .= " OR ";
            }

            $busca .= "tra.`data_prevista` >= '" . $this->_helper->inverterData(addslashes($_POST['data_prevista_inicio']), '/', '-') . "'";

        }

        if( !empty($_POST['data_prevista_fim']) ){
            if( !empty($busca) ){
                $busca .= " OR ";
            }

            $busca .= "tra.`data_prevista` <= '" . $this->_helper->inverterData(addslashes($_POST['data_prevista_fim']), '/', '-') . "'";

        }


        if( !empty($_POST['data_realizada_inicio']) ){
            if( !empty($busca) ){
                $busca .= " OR ";
            }

            $busca .= "tra.`data_realizada` >= '" . $this->_helper->inverterData(addslashes($_POST['data_realizada_inicio']), '/', '-') . "'";

        }

        if( !empty($_POST['data_realizada_fim']) ){
            if( !empty($busca) ){
                $busca .= " OR ";
            }

            $busca .= "tra.`data_realizada` <= '" . $this->_helper->inverterData(addslashes($_POST['data_prevista_fim']), '/', '-') . "'";

        }

//        echo $busca;
//        die;




        $transacao 	= new Transacao();
        $retorno 	= $transacao->pesquisa($busca);
        require_once "view/extrato/buscar.php";

    }






}
