<?php
if (!session_id()) {
	session_start();

}

require_once dirname(__FILE__) . '/../model/Contas.php';
require_once dirname(__FILE__) . '/../model/Usuarios.php';
require_once dirname(__FILE__) . '/../controller/HelperController.php';

class ContasController
{

	private $_actionCreate = '/?class=contas&acao=create';
	private $_actionIndex = '/?class=contas&acao=index';
    private $_actionEdit = '/?class=contas&acao=edit';

    protected $_helper;


	public function __construct()
	{
        $this->_helper = new HelperController();
        $this->_helper->userLogin();
	}


    /**
     * @param $redirect
     * @param bool $update
     * Valida os campos via posts
     */
    public function validaCampos($redirect)
    {
        $_SESSION['campos'] = $_POST;

        if( empty($_POST['empresa']) || empty($_POST['responsavel']) || empty($_POST['telefone']) || empty($_POST['cnpj']) ){
            $_SESSION['message'] = ['txt' => 'Todos os campos são obrigatórios', 'status' => 'error'];
            header("LOCATION: " . $redirect);
            exit();

        }


    }


	public function index()
	{

//        echo "<pre>";
//        print_r($_SESSION['user_login']);
//        echo "</pre>";



		$contas 	= new Contas();

        if($_SESSION['user_login']['nivel'] == 1){ // admin
            $retorno 	= $contas->buscarTodos();


        } else {
            $retorno 	= $contas->buscarTodosPorUser($_SESSION['user_login']['id']);


        }

//        die;

		require_once "view/contas/index.php";
		exit();
	}


	public function create()
	{
        $usuario        = new Usuarios();
        $dadosUsuarios  = $usuario->buscarTodos();

		require_once "view/contas/create.php";
		exit();

	}


	public function store()
	{

        $this->validaCampos($this->_actionCreate);

//        echo "<pre>";
//        print_r($_POST);
//        echo "</pre>";
//        die;
        $id_usuario     = !empty($_POST['id_usuario']) ? $_POST['id_usuario'] : $_SESSION['user_login']['id'];

		$contas 		= new Contas($id_usuario, $_POST['responsavel'], $_POST['empresa'], $_POST['telefone'], $_POST['cnpj'], 1);
		$retornoConta 	= $contas->existeConta(); // verifica se existe usuário com mesmo CNPJ

		if( $retornoConta == 0 ){

			$retorno 	= $contas->insert();

			if($retorno['status']){
				$_SESSION['message'] = ['txt' => "Conta cadastrado com sucesso!"];
				header("LOCATION: " . $this->_actionIndex);
				exit();
			}


			$_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
			header("LOCATION: " . $this->_actionCreate);
			exit();

		}

		$_SESSION['message'] = ['txt' => 'Conta já existe no banco de dados', 'status' => 'error'];
		header("LOCATION: " . $this->_actionCreate);
		exit();

	}


    public function edit()
    {
        if( isset($_GET['id']) && !empty($_GET['id']) ){

            $contas 	= new Contas();
            $retorno 	= $contas->buscarPorId($_GET['id']);
            $dados      = $retorno->fetch_assoc();

            $usuario        = new Usuarios();
            $dadosUsuarios  = $usuario->buscarTodos();

            require_once "view/contas/edit.php";
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();


    }


    public function update()
    {
        if( isset($_POST['id']) && !empty($_POST['id']) ){

            $action = $this->_actionEdit . "&id=".$_POST['id'];
            $this->validaCampos($action, true);

//                    echo "<pre>";
//            print_r($_POST);
//            echo "</pre>";
//            die;

            $id_usuario     = !empty($_POST['id_usuario']) ? $_POST['id_usuario'] : $_SESSION['user_login']['id'];

            $contas 		= new Contas($id_usuario, $_POST['responsavel'], $_POST['empresa'], $_POST['telefone'], $_POST['cnpj'],$_POST['status']);
            $retorno 	    = $contas->update($_POST['id']);

            if($retorno['status']){
                $_SESSION['message'] = ['txt' => "Conta atualizada com sucesso!"];
                header("LOCATION: " . $this->_actionIndex);
                exit();
            }


            $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
            header("LOCATION: " . $this->_actionCreate);
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();

    }


    public function delete()
    {
        if( isset($_GET['id']) && !empty($_GET['id']) ){
            $contas 	= new Contas();
            $retorno 	= $contas->delete($_GET['id']);

            if($retorno['status']){
                $_SESSION['message'] = ['txt' => "Conta deletado com sucesso!"];
                header("LOCATION: " . $this->_actionIndex);
                exit();
            }


            $_SESSION['message'] = ['txt' => $retorno['txt'], 'status' => 'error'];
            header("LOCATION: " . $this->_actionIndex);
            exit();

        }

        $_SESSION['message'] = ['txt' => 'Campo ID inexistente', 'status' => 'error'];
        header("LOCATION: " . $this->_actionIndex);
        exit();

    }
}
