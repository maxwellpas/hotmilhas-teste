<?php
require_once dirname(__FILE__) . '/../inc/head.php';
?>
<!-- Custom styles for this template -->
<link href="../assets/css/signin.css" rel="stylesheet">
</head>

<body>
<form class="form-signin" name="frm" id="frm" method="post" action="/?class=home&acao=login">
    <div class="text-center mb-4">
        <img class="mb-4 img-fluid logo" src="../assets/images/logo-hotmilhas.png" alt="" />
        <h1 class="mb-3">Erro 404</h1>
        <p>Classe não encontrada</p>
        <a href="/?class=painel&acao=index">Ir para o painel</a>
    </div>



    <p class="mt-5 mb-3 text-muted text-center">&copy; Hotmilhas-2018</p>
</form>

<?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>