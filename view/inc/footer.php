    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/popper.min.js"></script>
    <script src="../../assets/js/bootstrap.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script src="../../assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript">
        feather.replace()


         $(document).ready(function(){
             $('.excluir').click(function(e){
                 e.preventDefault();

                 if(confirm("Deseja realmente excluir?")){
                     var redirecionarParaExcluir = $(this).data('acao');
                     window.location.href = redirecionarParaExcluir;
                 }

             });


             $('.date').mask('00/00/0000');
             $('.phone').mask('(00) 90000-0000');
             $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
             $('.money').mask("#.##0,00", {reverse: true});
         });
     </script>

  </body>
</html>
