<?php require_once dirname(__FILE__) . '/../../controller/HelperController.php';?>
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link <?php echo ('painel' == $_GET['class']) ? 'active' : ''?>" href="/?class=painel&acao=index">
                    <span data-feather="home"></span> Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>

            <?php
            // somente administrador
            if($_SESSION['user_login']['nivel'] == 1):
            ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo ('usuario' == $_GET['class']) ? 'active' : ''?>" href="/?class=usuario&acao=index">
                        <span data-feather="users"></span> Usuários
                    </a>
                </li>
            <?php endif;?>


            <li class="nav-item">
                <a class="nav-link <?php echo ('contas' == $_GET['class']) ? 'active' : ''?>" href="/?class=contas&acao=index">
                    <span data-feather="file"></span> Contas
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?php echo ('transacao' == $_GET['class']) ? 'active' : ''?>" href="/?class=transacao&acao=index">
                    <span data-feather="bar-chart-2"></span> Transações
                </a>
            </li>
            <?php
            // somente administrador
            if($_SESSION['user_login']['nivel'] == 1):
            ?>
            <li class="nav-item">
                <a class="nav-link <?php echo ('extrato' == $_GET['class']) ? 'active' : ''?>" href="/?class=extrato&acao=index">
                    <span data-feather="bar-chart-2"></span> Extrato Financeiro
                </a>
            </li>
            <?php endif;?>
        </ul>

    </div>
</nav>
