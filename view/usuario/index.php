    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Usuários</h2>

                <?php 

//                echo '<pre>';
//                print_r($_POST);
//                print_r($_SESSION);
//                echo '</pre>';

                echo $helper->showMessage();


                ?>


                <div class="table-responsive">

                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=usuario&acao=create" class="btn btn-sm btn-outline-secondary">Novo Usuário</a>
                    </div>

                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                              <th>#</th>
                              <th>Nome</th>
                              <th>E-mail</th>
                              <th>Status</th>
                              <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if($retorno):
                                $k = 1;
                                while($dados = $retorno->fetch_assoc()):
//                                    echo '<pre>';
//                                    print_r($dados);
//                                    echo '</pre>';
                            ?>
                                    <tr>
                                      <td><?php echo $k;?></td>
                                      <td>
                                          <?php echo $dados['nome']?><br />
                                          <?php echo $helper->showNivel($dados['nivel'])?>
                                      </td>
                                      <td><?php echo $dados['email']?></td>
                                      <td><?php echo $helper->showStatus($dados['status'])?></td>
                                      <td>
                                          <a href="/?class=usuario&acao=edit&id=<?php echo $dados['id']?>" class="btn btn-secondary btn-sm">editar</a>
                                          <?php if( $dados['id'] != $_SESSION['user_login']['id']):?>
                                            <a href="#" data-acao="/?class=usuario&acao=delete&id=<?php echo $dados['id']?>" class="btn btn-danger btn-sm excluir">excluir</a>
                                          <?php endif;?>
                                      </td>
                                    </tr>
                            <?php
                                $k++;
                                endwhile;
                            else:
                            ?>
                                <tr>
                                    <td colspan="5" class="text-center p-3">Nenhum dado cadastrado.</td>
                                </tr>
                            <?php
                            endif;
                            ?>

                        </tbody>
                    </table>
                </div>
            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>

