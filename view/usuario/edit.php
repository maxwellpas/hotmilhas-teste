    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Usuários - Editar</h2>


                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=usuario&acao=create" class="btn btn-sm btn-outline-secondary">Novo Usuário</a>
                        <a  href="/?class=usuario&acao=index" class="btn btn-sm btn-outline-secondary">voltar</a>
                    </div>

                    <?php 

//                    echo '<pre>';
//                    print_r($_POST);
//                    print_r($_SESSION['campos']);
//                    print_r($dados);
//                    echo '</pre>';

                    echo $helper->showMessage();

                    ?>



                    <form name="frm" id="frm" method="post" action="/?class=usuario&acao=update&id=<?php echo $dados['id']?>">
                        <input type="hidden" name="id" value="<?php echo $dados['id']?>"/>
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $dados['nome']?>" placeholder="Nome completo" required>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $dados['email']?>" placeholder="Informar um e-mail válido" required>
                            </div>
                             <div class="form-group col-md-6">
                                <label for="email_confirm">Confirmar e-mail</label>
                                <input type="email" class="form-control" id="email_confirm" name="email_confirm" value="<?php echo $dados['email']?>" placeholder="Informar um e-mail válido" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="senha">Senha</label>
                                <input type="password" class="form-control" id="senha" name="senha" value="" placeholder="Senha com mínimo 6 caracateres" >
                                <span>Para alterar a senha preencha os campos</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="senha_confirm">Confirmar senha</label>
                                <input type="password" class="form-control" id="senha_confirm" name="senha_confirm" value="" placeholder="Senha com mínimo 6 caracateres" >
                                <span>Para alterar a senha preencha os campos</span>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nivel">Nivel</label>
                                <?php echo $helper->blockNivelUser($dados, 1, $_SESSION['user_login']['id']);?>
                                    <select id="nivel" name="nivel" class="form-control" required>
                                        <option value="1" <?php echo $helper->blockNivelUser($dados, 1, $_SESSION['user_login']['id']);?> >Administrador</option>
                                        <option value="2" <?php echo $helper->blockNivelUser($dados, 2, $_SESSION['user_login']['id']);?>>Usuário</option>
                                    </select>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="nivel">Nivel</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="status" name="status" <?php echo $dados['status'] == 1 ? 'checked' :  ''?>>
                                    <label class="form-check-label" for="status">
                                        Ativar?
                                    </label>
                                </div>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <a href="/?class=usuario&acao=index" class="btn btn-light">Voltar</a>
                    </form>




            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>