    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Usuários - Incluir</h2>


                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=usuario&acao=index" class="btn btn-sm btn-outline-secondary">voltar</a>
                    </div>

                    <?php
//                    echo '<pre>';
//                    print_r($_POST);
//                    print_r($_SESSION['campos']);
//                    echo '</pre>';

                    echo $helper->showMessage();

                    ?>



                    <form name="frm" id="frm" method="post" action="/?class=usuario&acao=store">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="nome" name="nome" value="<?php echo $helper->showCampos('nome')?>" placeholder="Nome completo" required>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $helper->showCampos('email')?>" placeholder="Informar um e-mail válido" required>
                            </div>
                             <div class="form-group col-md-6">
                                <label for="email_confirm">Confirmar e-mail</label>
                                <input type="email" class="form-control" id="email_confirm" name="email_confirm" value="<?php echo $helper->showCampos('email_confirm')?>" placeholder="Informar um e-mail válido" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="senha">Senha</label>
                                <input type="password" class="form-control" id="senha" name="senha" value="<?php echo $helper->showCampos('senha')?>" placeholder="Senha com mínimo 6 caracateres" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="senha_confirm">Confirmar senha</label>
                                <input type="password" class="form-control" id="senha_confirm" name="senha_confirm" value="<?php echo $helper->showCampos('senha_confirm')?>" placeholder="Senha com mínimo 6 caracateres" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nivel">Nivel</label>
                                <select id="nivel" name="nivel" class="form-control" required>
                                    <option value="" selected>Selecione...</option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Usuário</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                        <a href="/?class=usuario&acao=index" class="btn btn-light">Voltar</a>
                    </form>




            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>