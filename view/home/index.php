    <?php
    require_once dirname(__FILE__) . '/../../controller/HelperController.php';
    require_once dirname(__FILE__) . '/../inc/head.php';
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/signin.css" rel="stylesheet">
</head>

<body>
    <form class="form-signin" name="frm" id="frm" method="post" action="/?class=home&acao=login">
        <div class="text-center mb-4">
            <img class="mb-4 img-fluid logo" src="../assets/images/logo-hotmilhas.png" alt="" />
            <h1 class="h3 mb-3 font-weight-normal">Faça seu login</h1>
        </div>

        <?php

        //                echo '<pre>';
        //                print_r($_POST);
        //                print_r($_SESSION);
        //                echo '</pre>';

        echo $helper->showMessage();
        ?>

        <div class="form-label-group">
            <input type="email" id="email" name="email" class="form-control" placeholder="E-mail válido" value="<?php echo $helper->showCampos('email')?>" required autofocus>
            <label for="email">E-mail</label>
        </div>

        <div class="form-label-group">
            <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" value="<?php echo $helper->showCampos('senha')?>" required>
            <label for="senha">Senha</label>
        </div>

<!--        <div class="checkbox mb-3 text-center">-->
<!--            <label><input type="checkbox" value="remember-me"> Lembrar me</label>-->
<!--        </div>-->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Fazer login</button>
        <p class="mt-5 mb-3 text-muted text-center">&copy; Hotmilhas-2018</p>
    </form>

<?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>