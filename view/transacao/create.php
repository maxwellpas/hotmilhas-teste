    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Transação - Incluir</h2>


                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=transacao&acao=index" class="btn btn-sm btn-outline-secondary">voltar</a>
                    </div>

                    <?php
//                    echo '<pre>';
//                    print_r($status->fetch_assoc());
//
//                    echo '</pre>';

                    echo $helper->showMessage();

                    ?>



                    <form name="frm" id="frm" method="post" action="/?class=transacao&acao=store">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="id_conta">Conta: *</label>
                                <select id="id_conta" name="id_conta" class="form-control" required>
                                    <option value="" selected>Selecione...</option>
                                    <?php while( $conta = $contas->fetch_assoc() ):?>
                                        <option value="<?php echo $conta['id']?>">Empr.: <?php echo $conta['empresa']?> -- Resp.: <?php echo $conta['nome_responsavel']?></option>
                                    <?php endwhile?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="titulo">Título da Transação: *</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $helper->showCampos('titulo')?>" placeholder="" required>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="valor">Valor: *</label>
                                <input type="text" class="form-control money" id="valor" name="valor" value="<?php echo $helper->showCampos('valor')?>" placeholder="" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="id_status_pgto">Status do Pagamento:</label>
                                <select id="id_status_pgto" name="id_status_pgto" class="form-control" required>
                                    <?php while( $statu = $status->fetch_assoc() ):?>
                                        <option value="<?php echo $statu['id']?>"><?php echo $statu['titulo']?></option>
                                    <?php endwhile?>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="data_prevista">Data Prevista: *</label>
                                <input type="text" class="form-control date" id="data_prevista" name="data_prevista" value="<?php echo $helper->showCampos('data_prevista')?>"  placeholder="__/__/____" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="data_realizada">Data Realizada:</label>
                                <input type="text" class="form-control date" id="data_realizada" name="data_realizada" value="<?php echo $helper->showCampos('data_realizada')?>"  placeholder="__/__/____">
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                        <a href="/?class=transacao&acao=index" class="btn btn-light">Voltar</a>
                    </form>




            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>