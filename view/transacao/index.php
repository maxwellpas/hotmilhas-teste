    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Transação</h2>

                <?php 

//                echo '<pre>';
//                print_r($retorno->fetch_assoc());
//                print_r($_SESSION);
//                echo '</pre>';
//
//                die;

                echo $helper->showMessage();


                ?>


                <div class="table-responsive">

                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=transacao&acao=create" class="btn btn-sm btn-outline-secondary">Criar uma nova transação</a>
                    </div>

                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Conta</th>
                                <th>Título</th>
                                <th>valor</th>
                                <th>Status Pagamento</th>
                                <th>Data Prevista - Data Realizada</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if($retorno):
                                $k = 1;
                                while($dados = $retorno->fetch_assoc()):
//                                    echo '<pre>';
//                                    print_r($dados);
//                                    echo '</pre>';
                            ?>
                                    <tr>
                                        <td><?php echo $k;?></td>
                                        <td><?php echo $dados['empresa_conta']?></td>
                                        <td><?php echo $dados['titulo_transacao']?></td>
                                        <td><?php echo number_format($dados['valor_transacao'],2,",",".")?></td>
                                        <td><?php echo $dados['titulo_status_pgto']?></td>
                                        <td><?php echo $helper->inverterData($dados['data_prevista_transacao'], "-", "/")?> - <?php echo $helper->inverterData($dados['data_realizada_transacao'], "-", "/")?></td>
                                        <td>
                                            <a href="/?class=transacao&acao=edit&id=<?php echo $dados['id_transacao']?>" class="btn btn-secondary btn-sm">editar</a>
                                            <a href="#" data-acao="/?class=transacao&acao=delete&id=<?php echo $dados['id_transacao']?>" class="btn btn-danger btn-sm excluir">excluir</a>
                                        </td>
                                    </tr>
                            <?php
                                $k++;
                                endwhile;
                            else:
                            ?>
                                <tr>
                                    <td colspan="6" class="text-center p-3">Nenhum dado cadastrado.</td>
                                </tr>
                            <?php
                            endif;
                            ?>

                        </tbody>
                    </table>
                </div>
            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>

