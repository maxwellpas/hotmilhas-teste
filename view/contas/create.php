    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Contas - Incluir</h2>


                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=contas&acao=index" class="btn btn-sm btn-outline-secondary">voltar</a>
                    </div>

                    <?php
//                    echo '<pre>';
//                    print_r($dadosUsuarios->fetch_assoc());
//                    print_r($_SESSION['campos']);
//                    echo '</pre>';

                    echo $helper->showMessage();

                    ?>



                    <form name="frm" id="frm" method="post" action="/?class=contas&acao=store">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="empresa">Empresa:</label>
                                <input type="text" class="form-control" id="empresa" name="empresa" value="<?php echo $helper->showCampos('empresa')?>" placeholder="" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="responsavel">Responsável (contato na empresa):</label>
                                <input type="text" class="form-control" id="responsavel" name="responsavel" value="<?php echo $helper->showCampos('responsavel')?>" placeholder="" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="telefone">Telefone:</label>
                                <input type="text" class="form-control phone" id="telefone" name="telefone" value="<?php echo $helper->showCampos('telefone')?>" placeholder="(__) _____-____" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="cnpj">CNPJ:</label>
                                <input type="text" class="form-control cnpj" id="cnpj" name="cnpj" value="<?php echo $helper->showCampos('cnpj')?>" placeholder="__.___.___/____-__" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="id_usuario">Usuário (para atribuir a conta):</label>
                                <select id="id_usuario" name="id_usuario" class="form-control">
                                    <option value="" selected>Selecione...</option>
                                    <?php while($user = $dadosUsuarios->fetch_assoc()):?>
                                        <option value="<?php echo $user['id']?>"><?php echo $user['nome']?></option>
                                    <?php endwhile;?>
                                </select>
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                        <a href="/?class=contas&acao=index" class="btn btn-light">Voltar</a>
                    </form>




            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>