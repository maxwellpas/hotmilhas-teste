    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Contas - Editar</h2>


                    <div class="btnAcoes mb-4 mt-3">
                        <a  href="/?class=contas&acao=create" class="btn btn-sm btn-outline-secondary">Nova conta</a>
                        <a  href="/?class=contas&acao=index" class="btn btn-sm btn-outline-secondary">voltar</a>
                    </div>

                    <?php 

//                    echo '<pre>';
//                    print_r($_POST);
//                    print_r($_SESSION['campos']);
//                    print_r($dados);
//                    echo '</pre>';

                    echo $helper->showMessage();

                    ?>

                    <form name="frm" id="frm" method="post" action="/?class=contas&acao=update&id=<?php echo $dados['id']?>">
                        <input type="hidden" name="id" value="<?php echo $dados['id']?>"/>


                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="empresa">Empresa:</label>
                                <input type="text" class="form-control" id="empresa" name="empresa" value="<?php echo $dados['empresa']?>" placeholder="" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="responsavel">Responsável (contato na empresa):</label>
                                <input type="text" class="form-control" id="responsavel" name="responsavel" value="<?php echo $dados['nome_responsavel']?>" placeholder="" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="telefone">Telefone:</label>
                                <input type="text" class="form-control phone" id="telefone" name="telefone" value="<?php echo $dados['telefone']?>" placeholder="(__) _____-____" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="cnpj">CNPJ:</label>
                                <input type="text" class="form-control cnpj" id="cnpj" name="cnpj" value="<?php echo $dados['cnpj']?>" placeholder="__.___.___/____-__" required>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nivel">Nivel:</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="1" id="status" name="status" <?php echo $dados['status'] == 1 ? 'checked' :  ''?>>
                                    <label class="form-check-label" for="status">
                                        Ativar?
                                    </label>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="id_usuario">Usuário (para atribuir a conta):</label>
                                <select id="id_usuario" name="id_usuario" class="form-control">
                                    <option value="" selected>Selecione...</option>
                                    <?php while($user = $dadosUsuarios->fetch_assoc()):?>
                                        <option value="<?php echo $user['id']?>" <?php echo $dados['id_usuario'] == $user['id'] ? 'selected' :  ''?>><?php echo $user['nome']?></option>
                                    <?php endwhile;?>
                                </select>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <a href="/?class=contas&acao=index" class="btn btn-light">Voltar</a>
                    </form>




            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>