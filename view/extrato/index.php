    <?php 
    require_once dirname(__FILE__) . '/../../controller/HelperController.php'; 
    require_once dirname(__FILE__) . '/../inc/head.php'; 
    $helper = new HelperController();
    ?>
    <!-- Custom styles for this template -->
    <link href="../assets/css/dashboard.css" rel="stylesheet">


    <link href="../assets/css/style.css" rel="stylesheet">
</head>

    <body>
        <?php require_once dirname(__FILE__) . '/../inc/center-nav.php' ?>

    <div class="container-fluid">
        <div class="row">

            <?php require_once dirname(__FILE__) . '/../inc/sidebar.php' ?>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                <?php require_once dirname(__FILE__) . '/../inc/center-header.php' ?>

                <h2>Extrato</h2>

                <?php 

//                echo '<pre>';
//                print_r($retorno->fetch_assoc());
//                print_r($_SESSION);
//                echo '</pre>';
//
//                die;

                echo $helper->showMessage();


                ?>


                <div class="table-responsive">


                    <form name="frm" id="frm" method="post" action="/?class=extrato&acao=buscar">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="id_conta">Conta:</label>
                                <select id="id_conta" name="id_conta" class="form-control" >

                                    <?php while( $conta = $contas->fetch_assoc() ):?>
                                        <option value="<?php echo $conta['id']?>" ><?php echo $conta['empresa']?></option>
                                    <?php endwhile?>
                                </select>
                            </div>
<!--                            <div class="form-group col-md-4">-->
<!--                                <label for="titulo">Título da Transação:</label>-->
<!--                                <input type="text" class="form-control" id="titulo" name="titulo" value="" placeholder="" >-->
<!--                            </div>-->
<!--                            <div class="form-group col-md-4">-->
<!--                                <label for="id_status_pgto">Status do Pagamento:</label>-->
<!--                                <select id="id_status_pgto" name="id_status_pgto" class="form-control" >-->
<!--                                    --><?php //while( $statu = $status->fetch_assoc() ):?>
<!--                                        <option value="--><?php //echo $statu['id']?><!--">--><?php //echo $statu['titulo']?><!--</option>-->
<!--                                    --><?php //endwhile?>
<!--                                </select>-->
<!--                            </div>-->
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="data_prevista_inicio">Data Prevista - Início:</label>
                                <input type="text" class="form-control date" id="data_prevista_inicio" name="data_prevista_inicio" value="" placeholder="__/__/____" >
                            </div>
                            <div class="form-group col-md-3">
                                <label for="data_prevista_fim">Data Prevista - Fim:</label>
                                <input type="text" class="form-control date" id="data_prevista_fim" name="data_prevista_fim" value="" placeholder="__/__/____"   >
                            </div>
                            <div class="form-group col-md-3">
                                <label for="data_realizada_inicio">Data Realizada - Início:</label>
                                <input type="text" class="form-control date" id="data_realizada_inicio" name="data_realizada_inicio" value="" placeholder="__/__/____"  >
                            </div>
                            <div class="form-group col-md-3">
                                <label for="data_realizada_fim">Data Realizada - Fim:</label>
                                <input type="text" class="form-control date" id="data_realizada_fim" name="data_realizada_fim" value="" placeholder="__/__/____"    >
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Buscar</button>
                    </form>



                </div>
            </main>
      </div>
    </div>

   <?php require_once  dirname(__FILE__) . '/../inc/footer.php' ?>

