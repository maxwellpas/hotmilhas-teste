CREATE TABLE `hotfinanc_usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL,
  `nivel` int(1) DEFAULT '2' COMMENT '1 - ADMIN, 2 USER',
  `status` set('0','1') DEFAULT '1',
  `lixo` set('0','1') DEFAULT '0',
  `data_ultimo_login` datetime DEFAULT NULL,
  `data_cadastro` datetime DEFAULT CURRENT_TIMESTAMP,
  `data_edicao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `hotfinanc_usuarios` (`id`, `nome`, `email`, `senha`, `nivel`, `status`, `lixo`, `data_ultimo_login`, `data_cadastro`, `data_edicao`)
VALUES
	(2,'Maxwell Pereira Alves de Sousa','maxwellpas@gmail.com','4297f44b13955235245b2497399d7a93',1,'1','0','0000-00-00 00:00:00','2018-07-06 22:04:13',NULL),
	(3,'Maxwell Pereira Alves de Sousa','maxwellpass@gmail.com','4297f44b13955235245b2497399d7a93',1,'','1',NULL,'2018-07-06 22:05:21',NULL),
	(4,'teste','teste@teste.com.br','4297f44b13955235245b2497399d7a93',2,'1','0','0000-00-00 00:00:00','2018-07-06 22:57:00',NULL),
	(5,'fufu','testee@teste.com.br','d41d8cd98f00b204e9800998ecf8427e',1,'','1',NULL,'2018-07-06 22:58:22',NULL),
	(6,'Outro teste','maxwellpas@yahoo.com.br','4297f44b13955235245b2497399d7a93',2,'1','0',NULL,'2018-07-08 20:10:00',NULL),
	(7,'Usuário Hotmilhas','admin@hotmilhas.com.br','e10adc3949ba59abbe56e057f20f883e',1,'1','0','0000-00-00 00:00:00','2018-07-08 20:58:32',NULL);

CREATE TABLE `hotfinanc_contas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) unsigned NOT NULL,
  `nome_responsavel` varchar(255) DEFAULT NULL,
  `empresa` varchar(255) DEFAULT NULL,
  `telefone` char(20) DEFAULT NULL,
  `cnpj` char(40) DEFAULT NULL,
  `status` set('0','1') NOT NULL DEFAULT '1',
  `lixo` set('0','1') NOT NULL DEFAULT '0',
  `data_cadastro` datetime DEFAULT CURRENT_TIMESTAMP,
  `data_edicao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotfinanc_contas_id_usuario` (`id_usuario`),
  CONSTRAINT `fk_hotfinanc_contas_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `hotfinanc_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `hotfinanc_contas` (`id`, `id_usuario`, `nome_responsavel`, `empresa`, `telefone`, `cnpj`, `status`, `lixo`, `data_cadastro`, `data_edicao`)
VALUES
	(1,2,'Fulano tal','Hotimilhas','(11) 11112-3424','22.222.324/2234-23','1','0','2018-07-08 09:12:22',NULL),
	(2,4,'Maxwell','Prefeitura de Contagem','(98) 72424-2423','89.723.423/4342-42','1','0','2018-07-08 09:48:58',NULL),
	(3,2,'fksjlfdjl','fufu','9808890','9890','1','1','2018-07-08 09:49:06',NULL),
	(4,4,'Fulano','maxwellpas.com.br','(31) 99798-5851','42.343.242/4343-22','1','0','2018-07-08 14:14:07',NULL);


CREATE TABLE `hotfinanc_status_pgto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) DEFAULT NULL,
  `lixo` set('0','1') DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `hotfinanc_status_pgto` (`id`, `titulo`, `lixo`)
VALUES
	(1,'Pendente','0'),
	(2,'Pago','0');


CREATE TABLE `hotfinanc_transacoes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_conta` int(11) unsigned NOT NULL,
  `id_status_pgto` int(11) unsigned NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `data_prevista` datetime DEFAULT NULL,
  `data_realizada` datetime DEFAULT NULL,
  `lixo` set('0','1') DEFAULT '0',
  `data_cadastro` datetime DEFAULT CURRENT_TIMESTAMP,
  `data_edicao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotfinanc_transacoes_id_conta` (`id_conta`),
  KEY `fk_hotfinanc_status_pgto_id_status_pgto` (`id_status_pgto`),
  CONSTRAINT `fk_hotfinanc_status_pgto_id_status_pgto` FOREIGN KEY (`id_status_pgto`) REFERENCES `hotfinanc_status_pgto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotfinanc_transacoes_id_conta` FOREIGN KEY (`id_conta`) REFERENCES `hotfinanc_contas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `hotfinanc_transacoes` (`id`, `id_conta`, `id_status_pgto`, `titulo`, `valor`, `data_prevista`, `data_realizada`, `lixo`, `data_cadastro`, `data_edicao`)
VALUES
	(3,1,1,'Layout',2300,'0000-00-00 00:00:00','0000-00-00 00:00:00','1','2018-07-08 10:53:41',NULL),
	(4,2,1,'Layout',1.65,'2018-12-12 00:00:00','2018-11-11 00:00:00','0','2018-07-08 11:02:41','0000-00-00 00:00:00'),
	(5,2,1,'teste',1.65,'2018-12-12 00:00:00','0000-00-00 00:00:00','0','2018-07-08 11:06:15','0000-00-00 00:00:00'),
	(6,1,1,'Layout',1650,'2018-12-12 00:00:00','0000-00-00 00:00:00','0','2018-07-08 11:10:03',NULL),
	(7,2,1,'Layout kkslskksklskls',2987.98,'2018-12-12 00:00:00','2018-11-11 00:00:00','1','2018-07-08 11:10:40','0000-00-00 00:00:00'),
	(8,2,1,'2342',423423,'2018-12-12 00:00:00','2018-11-11 00:00:00','0','2018-07-08 14:45:07',NULL);



