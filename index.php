<?php
$classe     = ( isset($_GET['class']) && !empty($_GET['class']) ) ? ucfirst($_GET['class']) : 'Home';
$classe     = "{$classe}Controller";

if(!file_exists('controller/'.$classe.'.php')){
    require_once 'view/error/404.php';
    exit;
}

require_once 'controller/'.$classe.'.php';

if(!class_exists($classe)){
    require_once 'view/error/404-classe.php';
    exit;
}

$obj = new $classe();

$metodo     = ( isset($_GET['acao']) && !empty($_GET['acao']) ) ? $_GET['acao'] : 'index';

if(!method_exists($classe, $metodo)){
    require_once 'view/error/404-metodo.php';
    exit;
}


$obj->$metodo();
